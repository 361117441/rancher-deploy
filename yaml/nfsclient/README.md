此文件夹内的yaml文件是安装nfs-client，主要目的是为了实现PV的动态分配。
具体参考链接： https://www.jianshu.com/p/5e565a8049fc

1.pvc-client.yaml 部署nfs-client插件。
2.auth.yaml 如果集群启用了RBAC，则必须执行命令授权。(k8s1.6+默认开启)
3.nfs-client-class.yaml 创建StorageClass对象。（可以将storageClass对象设置为默认对象）
4.pvc.yaml 申请PV资源。

